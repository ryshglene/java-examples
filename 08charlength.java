import java.util.Scanner;

public class charlength {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter a single character: ");
        String userResponse = userInput.nextLine();
 
        System.out.print("\n");
        char userResponseChar = userResponse.charAt(0);

        for (int i = 0; i < userResponse.length(); i++ ) {
            if ( i > 0 ) {
                System.out.print("That's not a single character!\n");
                System.exit(0);
            }
        }

        System.out.print("Wow! I love that character '" + Character.toString(userResponseChar)+ "' too!\n");

    }
}
