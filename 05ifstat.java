import java.util.Scanner;

public class ifstat {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        String userYN;

        System.out.print("Enter yes or no [y/n]: ");
        userYN = userInput.nextLine();

        if ( userYN.equalsIgnoreCase("y") ) {
            System.out.print("Yes!\n");
        }
        else if ( userYN.equalsIgnoreCase("n") ) {
            System.out.print("No!\n");
        }
        else {
            System.out.print("Error!\n");
        }

    }
}
