import java.util.Scanner;

public class exitmsg {

    public static class ExitMessageSubrtn extends Thread {
        public void run() {
            System.out.println("\n\nExit commands go here!");
        }
    }

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        String reply;

        Runtime.getRuntime().addShutdownHook(new ExitMessageSubrtn());

        while (true) {
            System.out.print("Enter anything: ");
            reply = userInput.nextLine();
            System.out.print(reply + "\n");
        }

    }
}
