import java.util.Scanner;
import java.util.NoSuchElementException;

public class retryscope {
    public static void main(String[] args) {
        // Scanner objects don't like it when you
        // call next() after you've already set a
        // null value on them.
        // When you do so, they will throw an
        // uncatchable NoSuchElementException().
        // So put your scanner objects:
        // Not here!
        //Scanner userInput = new Scanner(System.in);

        System.out.print("\n\t[This is a Retry]\n");
        System.out.print("\n");

        while (true) {
            // But here!
            Scanner userInput = new Scanner(System.in);
            String userResponse = "";

            try {
                System.out.print("Ctrl+D: ");
                userResponse = userInput.nextLine();
            }
            catch (NoSuchElementException e) {
                System.out.print("Signal caught!\n");
                continue;
            }
        }

    }

