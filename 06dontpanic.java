public class dontpanic {
    public static void main(String[] args) {

        try {
            // Other data types do not throw an
            // exception when dividing by zero.
            // So we use an int variable.
            int a = 1, b = 0, c;
            c = a / b;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
