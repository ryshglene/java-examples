import java.util.Scanner;

public class casestat {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        String userYN;

        System.out.print("Enter yes or no [y/n]: ");
        userYN = userInput.nextLine();

        switch (userYN.toLowerCase()) {
            case "1":
            case "y":     System.out.print("Yes!\n");
                        break;
            case "0":
            case "n":     System.out.print("No!\n");
                        break;
            default:    System.out.print("Error!\n");   
                        break;
        }

    }
}
