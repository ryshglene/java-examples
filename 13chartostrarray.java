import java.util.Scanner;

public class chartostrarray {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter some text: ");
        char[] userResponse = userInput.nextLine().toCharArray();

        for (char c: userResponse) {
            System.out.print(c);
        }
        System.out.print("\n");

    }
}
